import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { Installation } from './installation';
import { INSTALLATIONS } from './mock-installations';
import { MessageService } from './message.service';

@Injectable()
export class InstallationService {

constructor(private messageService: MessageService) { }

  getInstallations(): Observable<Installation[]> {
    // Todo: send the message _after_ fetching the installations
    this.messageService.add('InstallationService: fetched installations');
    return of(INSTALLATIONS);
  }
}
