import { Component, OnInit, Input } from '@angular/core';
import { Installation } from '../installation';

@Component({
  selector: 'app-installation-detail',
  templateUrl: './installation-detail.component.html',
  styleUrls: ['./installation-detail.component.css']
})
export class InstallationDetailComponent implements OnInit {
  @Input() installation: Installation;

  constructor() { }

  ngOnInit() {
  }

}
