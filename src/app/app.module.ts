import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here

import { AppComponent } from './app.component';
import { InstallationsComponent } from './installations/installations.component';
import { InstallationDetailComponent } from './installation-detail/installation-detail.component';
import { InstallationService } from './installation.service';
import { MessagesComponent } from './messages/messages.component';
import { MessageService } from './message.service';

@NgModule({
  declarations: [
    AppComponent,
    InstallationsComponent,
    InstallationDetailComponent,
    MessagesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [ InstallationService, MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
