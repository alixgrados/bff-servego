import { Component, OnInit } from '@angular/core';

import { Installation } from '../installation';
import { InstallationService } from '../installation.service';

@Component({
  selector: 'app-installations',
  templateUrl: './installations.component.html',
  styleUrls: ['./installations.component.css']
})
export class InstallationsComponent implements OnInit {

  selectedInstallation: Installation;

  installations: Installation[];

  constructor(private installationService: InstallationService) { }

  ngOnInit() {
    this.getInstallations();
  }

  onSelect(installation: Installation): void {
    this.selectedInstallation = installation;
  }

  getInstallations(): void {
    this.installationService.getInstallations()
        .subscribe(installations => this.installations = installations);
  }
}
