FROM node:6.11.4

WORKDIR /app
ADD .angular-cli.json /app/
ADD package.json /app/
ADD protractor.conf.js /app/
ADD karma.conf.js /app/
ADD tsconfig.json /app/
ADD tslint.json /app/

ADD src /app/src 

RUN npm config set proxy http://ancy.proxy.corp.sopra:8080/
RUN npm install
RUN npm install -g @angular/cli




EXPOSE 4200

ENTRYPOINT ["ng", "serve"]
